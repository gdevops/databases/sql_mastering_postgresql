begin;

alter database appdev
  set search_path
   to chinook,
      f1db,
      geoname,
      magic,
      moma,
      opendata,
      sample,
      sandbox,
      twcache,
      tweet,
      public,
      raw;

create role cdstore with login in role appdev inherit;
create role f1db    with login in role appdev inherit;

alter role cdstore set search_path to chinook;
alter role f1db    set search_path to f1db;

commit;
