DUMP    = ./appdev.dump
PG_LIST = ./exclude-extensions.list

all: restore

restore: createdb $(PG_LIST)
	pg_restore -U appdev -d appdev --use-list=$(PG_LIST) $(DUMP)
	psql -U appdev -d appdev -f setup.sql

createdb:
	createuser -SDr appdev
	createdb -O appdev appdev
	psql -d appdev -c 'create extension btree_gist'

dropdb:
	dropdb --if-exists appdev
	dropuser --if-exists appdev
	dropuser --if-exists cdstore
	dropuser --if-exists f1db

$(PG_LIST): $(DUMP)
	pg_restore --list $(DUMP) | awk -f exclude-extensions.awk > $(PG_LIST)

.PHONY: all restore createdb dropdb
