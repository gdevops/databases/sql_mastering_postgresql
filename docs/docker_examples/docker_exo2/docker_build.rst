

.. seealso:: https://docs.docker.com/get-started/part2/#apppy



sudo docker build -t friendlyhello .
======================================


::
	
    pvergain@pvergain-MS-7721 /media/pvergain/14A02843A0282DA4/projets/exo_postgresql/docker_exo2 $ sudo docker build -t friendlyhello .
    
::
	    
    [sudo] Mot de passe de pvergain : 
		
::
			
	Sending build context to Docker daemon 4.608 kB
	Step 1/7 : FROM python:3.6
	3.6: Pulling from library/python
	85b1f47fba49: Pull complete 
	ba6bd283713a: Pull complete 
	817c8cd48a09: Pull complete 
	47cc0ed96dc3: Pull complete 
	4a36819a59dc: Pull complete 
	db9a0221399f: Pull complete 
	7a511a7689b6: Pull complete 
	1223757f6914: Pull complete 
	Digest: sha256:db9d8546f3ff74e96702abe0a78a0e0454df6ea898de8f124feba81deea416d7
	Status: Downloaded newer image for python:3.6
	 ---> 79e1dc9af1c1
	Step 2/7 : WORKDIR /app
	 ---> 7123fff53a0a
	Removing intermediate container 6eaf70db8019
	Step 3/7 : ADD . /app
	 ---> 790488c22bdc
	Removing intermediate container aa2f7daa4eae
	Step 4/7 : RUN pip install --trusted-host pypi.python.org -r requirements.txt
	 ---> Running in 8a77abc4cfc7
	Collecting Flask (from -r requirements.txt (line 1))
	  Downloading Flask-0.12.2-py2.py3-none-any.whl (83kB)
	Collecting Redis (from -r requirements.txt (line 2))
	  Downloading redis-2.10.6-py2.py3-none-any.whl (64kB)
	Collecting click>=2.0 (from Flask->-r requirements.txt (line 1))
	  Downloading click-6.7-py2.py3-none-any.whl (71kB)
	Collecting Werkzeug>=0.7 (from Flask->-r requirements.txt (line 1))
	  Downloading Werkzeug-0.13-py2.py3-none-any.whl (311kB)
	Collecting itsdangerous>=0.21 (from Flask->-r requirements.txt (line 1))
	  Downloading itsdangerous-0.24.tar.gz (46kB)
	Collecting Jinja2>=2.4 (from Flask->-r requirements.txt (line 1))
	  Downloading Jinja2-2.10-py2.py3-none-any.whl (126kB)
	Collecting MarkupSafe>=0.23 (from Jinja2>=2.4->Flask->-r requirements.txt (line 1))
	  Downloading MarkupSafe-1.0.tar.gz
	Building wheels for collected packages: itsdangerous, MarkupSafe
	  Running setup.py bdist_wheel for itsdangerous: started
	  Running setup.py bdist_wheel for itsdangerous: finished with status 'done'
	  Stored in directory: /root/.cache/pip/wheels/fc/a8/66/24d655233c757e178d45dea2de22a04c6d92766abfb741129a
	  Running setup.py bdist_wheel for MarkupSafe: started
	  Running setup.py bdist_wheel for MarkupSafe: finished with status 'done'
	  Stored in directory: /root/.cache/pip/wheels/88/a7/30/e39a54a87bcbe25308fa3ca64e8ddc75d9b3e5afa21ee32d57
	Successfully built itsdangerous MarkupSafe
	Installing collected packages: click, Werkzeug, itsdangerous, MarkupSafe, Jinja2, Flask, Redis
	Successfully installed Flask-0.12.2 Jinja2-2.10 MarkupSafe-1.0 Redis-2.10.6 Werkzeug-0.13 click-6.7 itsdangerous-0.24
	 ---> a7ecb362ac14
	Removing intermediate container 8a77abc4cfc7
	Step 5/7 : EXPOSE 80
	 ---> Running in 6eea186e2942
	 ---> 78336aa144e4
	Removing intermediate container 6eea186e2942
	Step 6/7 : ENV NAME World
	 ---> Running in 2888a4bc6a76
	 ---> f6a4405bbccd
	Removing intermediate container 2888a4bc6a76
	Step 7/7 : CMD python app.py
	 ---> Running in d6518ecd0f55
	 ---> a8fcef03530e
	Removing intermediate container d6518ecd0f55
	Successfully built a8fcef03530e
	
	

sudo docker images
====================	
	
::
		
    pvergain@pvergain-MS-7721 /media/pvergain/14A02843A0282DA4/projets/exo_postgresql/docker_exo2 $ sudo docker images
    
::
	    
	REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
	friendlyhello       latest              a8fcef03530e        4 minutes ago       701 MB
	hello-world         latest              f2a91732366c        2 weeks ago         1.85 kB
	python              3.6                 79e1dc9af1c1        5 weeks ago         691 MB


	
sudo docker run -p 4000:80 friendlyhello

	
	
	
	
