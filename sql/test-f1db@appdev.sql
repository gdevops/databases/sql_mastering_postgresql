\c postgres://f1db@/appdev

\ir 04-sql-select/04-sql-101/02_01.sql
\ir 04-sql-select/04-sql-101/02_02.sql
\ir 04-sql-select/04-sql-101/02_03.sql
\ir 04-sql-select/04-sql-101/02_02_01.sql
\ir 04-sql-select/04-sql-101/02_02_02.sql
\ir 04-sql-select/04-sql-101/02_03_01.sql
\ir 04-sql-select/04-sql-101/03_01.sql
\ir 04-sql-select/04-sql-101/03_02.sql
\ir 04-sql-select/04-sql-101/04_01.sql
\ir 04-sql-select/04-sql-101/04_02.sql
\ir 04-sql-select/04-sql-101/05_01_f1db.notfinishers.sql
\ir 04-sql-select/05-sql-102/01_01.sql
\ir 04-sql-select/05-sql-102/01_02.sql
\ir 04-sql-select/05-sql-102/01_03.sql
\ir 04-sql-select/05-sql-102/02_01.sql
\ir 04-sql-select/05-sql-102/02_02.sql
\ir 04-sql-select/05-sql-102/02_03.sql
\ir 04-sql-select/05-sql-102/03_01_f1db.decade.top3.sql
\ir 04-sql-select/05-sql-102/04_01.sql
\ir 04-sql-select/05-sql-102/04_02.sql
\ir 04-sql-select/06-sql-103/01_01.sql
\ir 04-sql-select/06-sql-103/01_02_f1db.decade.races.sql
\ir 04-sql-select/06-sql-103/01_03.sql
\ir 04-sql-select/06-sql-103/01_04.sql
\ir 04-sql-select/06-sql-103/02_01.sql
\ir 04-sql-select/06-sql-103/03_01.sql
\ir 04-sql-select/06-sql-103/04_01.sql
\ir 04-sql-select/06-sql-103/04_02.sql
\ir 04-sql-select/06-sql-103/04_03.sql
\ir 04-sql-select/06-sql-103/05_01.sql
\ir 04-sql-select/06-sql-103/05_02_f1db.accidents.sql
\ir 04-sql-select/06-sql-103/05_03_f1db.seasons.winners.sql
\ir 04-sql-select/06-sql-103/06_01.sql
\ir 04-sql-select/06-sql-103/06_02.sql
\ir 04-sql-select/06-sql-103/06_03.sql
\ir 04-sql-select/06-sql-103/07_01_f1db.standinds.union.sql
\ir 04-sql-select/06-sql-103/07_02_f1db.standinds.except.sql
\ir 04-sql-select/06-sql-103/07_03.sql
\ir 04-sql-select/07-nulls/01_01.sql
\ir 04-sql-select/07-nulls/01_02.sql
\ir 04-sql-select/07-nulls/03_01.sql
\ir 04-sql-select/07-nulls/04_01.sql
\ir 04-sql-select/07-nulls/04_02.sql
\ir 04-sql-select/08-window-functions/01_01.sql
\ir 04-sql-select/08-window-functions/01_02.sql
\ir 04-sql-select/08-window-functions/01_03.sql
\ir 04-sql-select/08-window-functions/01_04.sql
\ir 04-sql-select/08-window-functions/02_01.sql
\ir 04-sql-select/08-window-functions/03_01.sql
\ir 04-sql-select/09-relations/02_01.sql
\ir 05-data-types/02-relational/02_01.sql
\ir 05-data-types/02-relational/02_02.sql
\ir 05-data-types/02-relational/02_03.sql
\ir 05-data-types/03-pg-data-types-101/01_01.sql
\ir 05-data-types/03-pg-data-types-101/04_01.sql
\ir 05-data-types/03-pg-data-types-101/04_02.sql
\ir 06-data-modeling/06-denormalisation/04_01.sql

