
================================================
Mastering PostgreSQL in Application Development
================================================

Welcome to Mastering PostgreSQL in Application Development. Thanks for
having bought the Full Edition!

You will find here, first, the Book, in several formats:

  - MasteringPostgreSQLinAppDev.epub
  - MasteringPostgreSQLinAppDev.mobi
  - MasteringPostgreSQLinAppDev.pdf

The PDF has been optimized for reading on a Tablet, or on a computer. For
example, you can fit a two-pages side by side display of the PDF content on
a 13" screen!

The book contains:

  - 8 chapters and 45 sections
  - 5 interviews
  - 326 pages, PDF dimensions compatible with reading on a Tablet
  - 265 SQL queries
  - 12 datasets from the Real World™, with loading scripts
  - 56 tables in 13 schemas
  - 4 sample applications, with code written in Python, Go
  - Practical examples of data normalisation, with queries

Adding to the book, the Full Edition contains:

  - All queries available as SQL files
  - ToC file to match query files with book's ToC
  - Full runnable code from the applications
  - All the data pre-processing and loading scripts
  - a PostgreSQL database dump with all the book's data

Restoring the PostgreSQL database appdev
==========================================

A `Makefile` is included to ease the process, so that you may run the
following command and be done. Of course, the command assumes that you have
a PostgreSQL instance reachable with the default connection parameters set
in the environment. Those are `PGHOST`, `PGUSER` and etc, as documented in
the `man psql` manual page.

Feel free to have a look at the `Makefile` and hack something around if for
some reasons it doesn't work as intended in your specific environment.


How to produce the schemas for the database *appdev*
======================================================

A typical output looks like the following:

::

	createuser -SDr appdev

::
	
	createdb -O appdev appdev

::

	psql -d appdev -c 'create extension btree_gist'

	    CREATE EXTENSION
	    
::
	    
	pg_restore --list ./appdev.dump | awk -f exclude-extensions.awk > ./excluse-extensions.list
	
::
	
	pg_restore -U appdev -d appdev --use-list=./excluse-extensions.list ./appdev.dump
	
::
	
	psql -U appdev -d appdev -f setup.sql
	
	
	Password for user appdev: 
	Expanded display is used automatically.
	Null display is "¤".
	Line style is unicode.
	Unicode border line style is "single".
	Unicode column line style is "single".
	Unicode header line style is "double".
	SET
	begin;
	BEGIN
	alter database appdev
	  set search_path
	   to chinook,
		  f1db,
		  geoname,
		  magic,
		  moma,
		  opendata,
		  sample,
		  sandbox,
		  twcache,
		  tweet,
		  public,
		  raw;
	ALTER DATABASE
	create role cdstore with login in role appdev inherit;
	CREATE ROLE
	create role f1db    with login in role appdev inherit;
	CREATE ROLE
	alter role cdstore set search_path to chinook;
	ALTER ROLE
	alter role f1db    set search_path to f1db;
	ALTER ROLE
	commit;
	COMMIT


::

    appdev@appdev=#  \l

    
::
    
										List of databases
		   Name       │  Owner   │ Encoding │  Collate   │   Ctype    │   Access privileges   
	══════════════════╪══════════╪══════════╪════════════╪════════════╪═══════════════════════
	 appdev           │ appdev   │ UTF8     │ fr_FR.utf8 │ fr_FR.utf8 │ 
	 postgres         │ postgres │ UTF8     │ fr_FR.utf8 │ fr_FR.utf8 │ 
	 template0        │ postgres │ UTF8     │ fr_FR.utf8 │ fr_FR.utf8 │ =c/postgres          ↵
					  │          │          │            │            │ postgres=CTc/postgres
	 template1        │ postgres │ UTF8     │ fr_FR.utf8 │ fr_FR.utf8 │ =c/postgres          ↵
					  │          │          │            │            │ postgres=CTc/postgres
	 template_postgis │ postgres │ UTF8     │ fr_FR.utf8 │ fr_FR.utf8 │ 
	 test             │ pvergain │ UTF8     │ fr_FR.utf8 │ fr_FR.utf8 │ 
	(6 rows)

	appdev@appdev=#  \c appdev
	You are now connected to database "appdev" as user "appdev".
	appdev@appdev=#  \dt
					List of relations
	  Schema  │         Name         │ Type  │ Owner  
	══════════╪══════════════════════╪═══════╪════════
	 chinook  │ album                │ table │ appdev
	 chinook  │ artist               │ table │ appdev
	 chinook  │ customer             │ table │ appdev
	 chinook  │ employee             │ table │ appdev
	 chinook  │ genre                │ table │ appdev
	 chinook  │ invoice              │ table │ appdev
	 chinook  │ invoiceline          │ table │ appdev
	 chinook  │ mediatype            │ table │ appdev
	 chinook  │ playlist             │ table │ appdev
	 chinook  │ playlisttrack        │ table │ appdev
	 chinook  │ track                │ table │ appdev
	 f1db     │ circuits             │ table │ appdev
	 f1db     │ constructorresults   │ table │ appdev
	 f1db     │ constructors         │ table │ appdev
	 f1db     │ constructorstandings │ table │ appdev
	 f1db     │ drivers              │ table │ appdev
	 f1db     │ driverstandings      │ table │ appdev
	 f1db     │ laptimes             │ table │ appdev
	 f1db     │ pitstops             │ table │ appdev
	 f1db     │ qualifying           │ table │ appdev
	 f1db     │ races                │ table │ appdev
	 f1db     │ results              │ table │ appdev
	 f1db     │ seasons              │ table │ appdev
	 f1db     │ status               │ table │ appdev
	 geoname  │ class                │ table │ appdev
	 geoname  │ continent            │ table │ appdev
	 geoname  │ country              │ table │ appdev
	 geoname  │ district             │ table │ appdev
	 geoname  │ feature              │ table │ appdev
	 geoname  │ geoname              │ table │ appdev
	 geoname  │ neighbour            │ table │ appdev
	 geoname  │ region               │ table │ appdev
	 magic    │ allsets              │ table │ appdev
	 magic    │ cards                │ table │ appdev
	 magic    │ sets                 │ table │ appdev
	 opendata │ archives_planete     │ table │ appdev
	 public   │ access_log           │ table │ appdev
	 public   │ commitlog            │ table │ appdev
	 public   │ factbook             │ table │ appdev
	 public   │ hashtag              │ table │ appdev
	 public   │ hello                │ table │ appdev
	 public   │ rate                 │ table │ appdev
	 public   │ rates                │ table │ appdev
	 public   │ tweet                │ table │ appdev
	 raw      │ admin1               │ table │ appdev
	 raw      │ admin2               │ table │ appdev
	 sample   │ geonames             │ table │ appdev
	 sandbox  │ article              │ table │ appdev
	 sandbox  │ category             │ table │ appdev
	 sandbox  │ comment              │ table │ appdev
	 sandbox  │ lorem                │ table │ appdev
	 tweet    │ activity             │ table │ appdev
	 tweet    │ follower             │ table │ appdev
	 tweet    │ list                 │ table │ appdev
	 tweet    │ membership           │ table │ appdev
	 tweet    │ users                │ table │ appdev
	(56 rows)



The bundle ships with the `exclude-extensions.list` file, so that you won't
normally have the `pg_restore --list` line of output.

The `pg_restore --list` and `--use-list` options allow filtering SQL objects
from the dump that we don't want to restore. Here, we exclude extension
objects thanks to a very simple awk script.

PostgreSQL extensions can only be created by a superuser, and we use
pg_restore as the `appdev` user, the owner of our `appdev` database. It is
good practice that the owner of the database you're using is not a superuser
of your PostgreSQL instance.

So to avoid `pg_restore` time errors, we create the extension objects
previous to running the `pg_restore` command, as superusers, and then we
filter the commands out from the command thanks to the `--use-list` option.

If you have a CI/CD environment where you're restoring the most recent
nigthly production backup, this option may allow you to also filter out 
some SQL objects that shouldn't be copied to the CI/CD environment, 
such as production user credentials or other confidential information.

psql configuration
====================

You can install the `psqlrc` file to try it::

    $ cp -i psqlrc ~/.psqlrc
    

## Queries

All the queries from the book are available in the `sql/` directory. This
directory contains the `toc.txt` file that maps the SQL file names with the
Table of Content from the book, so have a look at it. Here's an extract of it:

~~~
  3 Writing SQL queries 
    3.1 Business Logic
      3.1.1 Every SQL query embeds some business logic
          03-writing-sql-queries/01-business-logic/01_01.sql
          03-writing-sql-queries/01-business-logic/01_02.sql
          03-writing-sql-queries/01-business-logic/01_03.sql
      3.1.2 Business Logic applies to Use Cases
          03-writing-sql-queries/01-business-logic/02_01.sql
          03-writing-sql-queries/01-business-logic/02_02_albums.py
      3.1.3 Correctness
      3.1.4 Efficiency
      3.1.5 Stored Procedures, a data access API
          03-writing-sql-queries/01-business-logic/05_01_album.pl.name.sql
          03-writing-sql-queries/01-business-logic/05_02_album.pl.id.sql
          03-writing-sql-queries/01-business-logic/05_03.sql
          03-writing-sql-queries/01-business-logic/05_04.sql
          03-writing-sql-queries/01-business-logic/05_05.sql
          03-writing-sql-queries/01-business-logic/05_06.sql
      3.1.6 Procedural Code and Stored Procedures
          03-writing-sql-queries/01-business-logic/06_01_album.pl.proc.sql
      3.1.7 So, where should I implement my business logic?
~~~

With that file, it should be easy to find the SQL queries you want to play
with.

~~~
$ cd sql
$ psql -U appdev -d appdev -f 03-writing-sql-queries/01-business-logic/01_01.sql
Expanded display is used automatically.
Null display is "⦱".
Null display is "¤".
Line style is unicode.
Unicode border line style is "single".
Unicode column line style is "single".
Unicode header line style is "double".
SET
             name             
══════════════════════════════
 The Power Of Equality
 If You Have To Ask
 Breaking The Girl
 Funky Monks
 Suck My Kiss
 I Could Have Lied
 Mellowship Slinky In B Major
 The Righteous & The Wicked
 Give It Away
 Blood Sugar Sex Magik
 Under The Bridge
 Naked In The Rain
 Apache Rose Peacock
 The Greeting Song
 My Lovely Man
 Sir Psycho Sexy
 They're Red Hot
(17 rows)
~~~

## Tests

When working on the book, I have of course tested the queries embedded in
the book pages. You can too run my test files directly:

~~~ bash
$ PAGER=cat psql -U appdev -d appdev -a -f sql/test-f1db@appdev.sql
~~~

## Conclusion

Your journey into *Mastering PostgreSQL in Application Development* is now
starting.

> _Knowledge is of no value unless you put it into practice._

> ***Anton Chekhov***

Writing code is fun. Have fun writing SQL!

